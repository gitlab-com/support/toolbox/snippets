# Collection of snippets used by the support team

First, a **warning**:

**This is the GitLab Support Team’s collection of information regarding the GitLab Rails console, for use while troubleshooting. It is listed here for transparency, and it may be useful for users with experience with these tools. If you are currently having an issue with GitLab, it is highly recommended that you check your [support options](https://about.gitlab.com/support/) first, before attempting to use this information.**

**CAUTION**: Please note that some of these scripts could be damaging if not run correctly, or under the right conditions. We highly recommend running them under the guidance of a Support Engineer, or running them in a test environment with a backup of the instance ready to be restored, just in case.

**CAUTION**: Please also note that as GitLab changes, changes to the code are inevitable, and so some scripts may not work as they once used to. These are not *yet* kept up-to-date as these scripts/commands were added as they were found/needed. As mentioned above, we recommend running these scripts under the supervision of a Support Engineer, who can also verify that they will continue to work as they should and, if needed, update the script for the latest version of GitLab.



Started [from a support-team-meta discussion](https://gitlab.com/gitlab-com/support/support-team-meta/issues/2087) as a first step to improve debugging / troubleshooting through having a SSOT for Support scripts.


Initially imported from the [existing Rails cheatsheet page](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html) with a [small custom script](https://gitlab.com/snippets/1945723).