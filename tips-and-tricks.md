# Tips & Tricks

This is a list of the various scripts, rake tasks, tools and techniques we often use at GitLab.

## Sidekiq

- [Troubleshooting Sidekiq in our docs](https://docs.gitlab.com/ee/administration/troubleshooting/sidekiq.html#remove-sidekiq-jobs-for-given-parameters-destructive).
